package com.example.spacedim.modeles

data class Action(
        val sentence: String,
        val uiElement: UIElement,
        val time: Long = 8000
)