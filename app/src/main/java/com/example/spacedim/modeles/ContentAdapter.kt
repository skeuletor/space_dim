package com.example.spacedim.modeles

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.PolymorphicJsonAdapterFactory
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory

object GameAdapter {
    val moshiAdapter = Moshi.Builder()
        .add(
            PolymorphicJsonAdapterFactory
                .of(Event::class.java, "type")
                .withSubtype(Event.GameStarted::class.java, EventType.GAME_STARTED.name)
                .withSubtype(Event.WaitingForPlayer::class.java, EventType.WAITING_FOR_PLAYER.name)
                .withSubtype(Event.GameOver::class.java, EventType.GAME_OVER.name)
                .withSubtype(Event.NextAction::class.java, EventType.NEXT_ACTION.name)
                .withSubtype(Event.NextLevel::class.java, EventType.NEXT_LEVEL.name)
                .withSubtype(Event.Error::class.java, EventType.ERROR.name)
                .withSubtype(Event.Ready::class.java, EventType.READY.name)
                .withSubtype(Event.PlayerAction::class.java, EventType.PLAYER_ACTION.name)

        )
        .add(
            PolymorphicJsonAdapterFactory
                .of(UIElement::class.java, "type")
                .withSubtype(UIElement.Button::class.java, UIType.BUTTON.name)
                .withSubtype(UIElement.Switch::class.java, UIType.SWITCH.name)
                .withSubtype(UIElement.Shake::class.java, UIType.SHAKE.name)
        )
        .add(KotlinJsonAdapterFactory())
        .build()

    val eventParser: JsonAdapter<Event> = moshiAdapter.adapter(Event::class.java)

    val UIElementParser: JsonAdapter<UIElement> = moshiAdapter.adapter(UIElement::class.java)

    val userListParser: JsonAdapter<User> = moshiAdapter.adapter(User::class.java)
}