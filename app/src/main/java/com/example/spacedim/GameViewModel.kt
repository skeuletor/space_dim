package com.example.spacedim

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.spacedim.modeles.*
import com.example.spacedim.network.EchoWebSocketListener
import okhttp3.*

class GameViewModel(incomingCurrentUserName: String, incomingCurrentUserId: Int) : ViewModel() {

    // ********************* WAITING ROOM VARIABLES ******************** \\

    private val client = OkHttpClient()
    val requestUri = "ws://spacedim.async-agency.com:8081/ws/join/"
    val listener = EchoWebSocketListener()
    private lateinit var ws: WebSocket

    private val currentUserName: String = incomingCurrentUserName
    private val currentUserId: Int = incomingCurrentUserId

    var currentSocket = socketState.DISCONNECTED

    private var room: String = ""

    private val _score = MutableLiveData<Int>()
    val score: LiveData<Int>
        get() = _score

    // The current word
    private val _contentText = MutableLiveData<String>()
    val contentText: LiveData<String>
        get() = _contentText

    private val _onMessage = MutableLiveData<String>()
    val onMessage: LiveData<String>
        get() = _onMessage

    // ********************* MAIN ******************** \\

    init {
        Log.i("GameViewModel", "GameViewModel created!")
        _contentText.value = R.string.waiting_empty_main_text.toString()
        _score.value = 0
        println("Current user from home: $currentUserName and $currentUserId")
    }


    // ********************* WAITING ROOM FUNCTIONS ******************** \\

    fun setRoom(room: String): Unit {
        this.room = room
        println("Room : $room")
    }

    fun joinRoom(room: String): Unit {
        startSocket(room)
    }

    fun setScore(score: Int) {
       _score.value = score
    }

    fun setPlayerReady() {
        var status = GameAdapter.eventParser.toJson(Event.Ready(true))
        ws.send(status)
    }

    fun getUsername(): String {
        return currentUserName
    }

    fun getUserId(): Int {
        return currentUserId
    }

    override fun onCleared() {
        super.onCleared()
        Log.i("GameViewModel", "GameViewModel destroyed!")
    }

    fun endGame(over: Event.GameOver) {
        setScore(over.score)
        ws.close(1000 , null)
    }

    fun startSocket(room: String): LiveData<Event> {
        val request = Request.Builder().url("$requestUri$room/$currentUserId").build()
        ws = client.newWebSocket(request, listener)
        //client.dispatcher.executorService.shutdown()
        return listener.eventMessage
    }



    // ********************* DASHBOARD FUNCTIONS ******************** \\

    fun dashboardBtnAction(element: UIElement) {
        println(element)
        val sent = Event.PlayerAction(element)
        ws.send(GameAdapter.eventParser.toJson(sent))
    }


}

enum class socketState {
    DISCONNECTED, CONNECTING, CONNECTED
}