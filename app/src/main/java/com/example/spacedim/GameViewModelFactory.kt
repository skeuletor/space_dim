package com.example.spacedim

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class GameViewModelFactory(private val incomingCurrentUserName: String, private val incomingCurrentUserId: Int) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(GameViewModel::class.java)) {
            return GameViewModel(incomingCurrentUserName, incomingCurrentUserId) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}