package com.example.spacedim.network

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.spacedim.modeles.Event
import com.example.spacedim.modeles.EventType
import com.example.spacedim.modeles.GameAdapter
import com.example.spacedim.modeles.UIElement
import okhttp3.Response
import okhttp3.WebSocket
import okhttp3.WebSocketListener
import okio.ByteString

class EchoWebSocketListener : WebSocketListener() {

    private val _eventMessage = MutableLiveData<Event>()
    val eventMessage: LiveData<Event>
        get() = _eventMessage

    private val _eventUi = MutableLiveData<List<UIElement>>()
    val eventUi: LiveData<List<UIElement>>
        get() = _eventUi

    override fun onOpen(webSocket: WebSocket, response: Response) {
        println("Socket open")
    }

    override fun onMessage(webSocket: WebSocket, text: String) {
        println("Receiving : $text")

        var mess: Event = GameAdapter.eventParser.fromJson(text)!!
        when (mess.type) {
            EventType.GAME_STARTED -> {
                mess as Event.GameStarted
                _eventUi.postValue(mess.uiElementList)
            }
            EventType.NEXT_LEVEL -> {
                mess as Event.NextLevel
                _eventUi.postValue(mess.uiElementList)
            }
        }
        _eventMessage.postValue(mess)

    }

    override fun onMessage(webSocket: WebSocket, bytes: ByteString) {
        println("Receiving bytes : " + bytes.hex())
    }


    override fun onClosing(webSocket: WebSocket, code: Int, reason: String) {
        _eventMessage.postValue(Event.Restart(true))
        webSocket.close(NORMAL_CLOSURE_STATUS, null)
        println("Closing : $code / $reason")
    }

    override fun onFailure(webSocket: WebSocket, t: Throwable, response: Response?) {
        println("Error : " + t.message)
    }

    companion object {
        private const val NORMAL_CLOSURE_STATUS = 4000
    }
}