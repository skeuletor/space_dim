package com.example.spacedim.network

import com.example.spacedim.modeles.User
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

private const val BASE_URL = "https://spacedim.async-agency.com/api/"

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .baseUrl(BASE_URL)
    .build()

interface UserApiService {
    @GET("user/find/{username}")
    suspend fun getUser(@Path("username") username: String?): User

    @GET("users?sort=top")
    suspend fun getBestScores(): List<User>

    @POST("user/register")
    suspend fun registerUser(@Body requestBody: RequestBody): Response<ResponseBody>
}

object UserApi {
    val retrofitService : UserApiService by lazy { retrofit.create(UserApiService::class.java) }
}

