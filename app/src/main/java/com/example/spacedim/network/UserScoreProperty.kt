package com.example.spacedim.network

import android.os.Parcelable
import com.example.spacedim.modeles.State
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserScoreProperty(
    val id: Int, val name: String, val avatar: String, var score: Int): Parcelable
