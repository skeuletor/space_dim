package com.example.spacedim

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.spacedim.databinding.ActivityMainBinding
import com.example.spacedim.modeles.GameAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.*
import okio.ByteString
import org.json.JSONStringer


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.supportActionBar?.hide();
        Log.i("MainActivity", "onCreate Called")
        //    @Suppress("UNUSED_VARIABLE")
        setContentView(R.layout.activity_main)
    }
}