package com.example.spacedim.screens.dashboard

import android.animation.ObjectAnimator
import android.os.Bundle

import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.LinearInterpolator
import android.widget.Button
import android.widget.TableRow
import android.widget.TextView
import android.widget.Toast
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.fragment.NavHostFragment
import com.example.spacedim.GameViewModel
import com.example.spacedim.R
import com.example.spacedim.databinding.DashboardBinding
import com.example.spacedim.modeles.*
import com.google.android.material.card.MaterialCardView
import com.google.android.material.switchmaterial.SwitchMaterial
import java.text.SimpleDateFormat
import java.util.*

class DashboardFragment : Fragment() {

    private lateinit var viewModel: GameViewModel
    private lateinit var binding : DashboardBinding
    private var firstUi = true

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        requireActivity().onBackPressedDispatcher.addCallback(this){
            return@addCallback
        }
        binding = DataBindingUtil.inflate<DashboardBinding>(inflater,
            R.layout.dashboard, container, false)

        viewModel = ViewModelProvider(requireActivity()).get(GameViewModel::class.java)
        binding.gameViewModel = viewModel

        binding.lifecycleOwner = viewLifecycleOwner

        viewModel.listener.eventMessage.observe(viewLifecycleOwner) { mess ->

            when (mess.type) {
                EventType.NEXT_ACTION -> {
                    println("Next action")
                    val action = mess as Event.NextAction
                    ObjectAnimator.ofInt(binding.progressBar, "progress", 100,0)
                        .setDuration(action.action.time)
//                        .setInterpolator(LinearInterpolator())
                        .start();

                        binding.dashTextView.text = action.action.sentence
                }
                EventType.GAME_OVER -> {
                    println("Game over")
                    val over = mess as Event.GameOver
                    viewModel.endGame(over)
                    gameOvered(over)
                }

            }
        }

        viewModel.listener.eventUi.observe(viewLifecycleOwner) { List ->

            createDashboardPannel(List)
            if (firstUi) {
                firstUi = false
            } else {
                Toast.makeText(context, "Nouveau level", Toast.LENGTH_SHORT).show()
            }
        }

        return binding.root
    }

    override fun onResume() {
        super.onResume()
        firstUi = true
        binding.dashboardPannel.removeAllViews()
    }

    private fun gameOvered(over: Event.GameOver) {
        val action = DashboardFragmentDirections.actionDashboardFragmentToEndGameFragment()
        action.score = over.score
        action.win = over.win
        action.level = over.level
        NavHostFragment.findNavController(this).navigate(action)
    }

    private fun createDashboardPannel(list : List<UIElement>) {
        val inflater = LayoutInflater.from(this.context)

        binding.dashboardPannel.removeAllViews()
        var nbButtonsOfRow: Int = 0
        var rowView = inflater.inflate(R.layout.tablerow, null) as TableRow

        for (element in list) {
            if (nbButtonsOfRow >= 2) {
                nbButtonsOfRow = 0
                binding.dashboardPannel.addView(rowView)
                rowView = inflater.inflate(R.layout.tablerow, null) as TableRow

            }
            if (element.type === UIType.BUTTON || element.type === UIType.SHAKE) {
                val button = inflater.inflate(R.layout.standard_button, null) as Button
                button.text = element.content
                button.id = element.id
                button.setOnClickListener {
                    viewModel.dashboardBtnAction(element)
                }
                rowView.addView(button)
                nbButtonsOfRow += 1
            }
            if (element.type === UIType.SWITCH ) {
                val card = inflater.inflate(R.layout.switch_button, null) as MaterialCardView
                card.findViewById<TextView>(R.id.player_1_name).text = element.content
                card.id = element.id
                card.findViewById<SwitchMaterial>(R.id.switch_material).setOnClickListener{
                    viewModel.dashboardBtnAction(element)
                }
                rowView.addView(card)
                nbButtonsOfRow += 1
            }

        }
        binding.dashboardPannel.addView(rowView)
    }

}
