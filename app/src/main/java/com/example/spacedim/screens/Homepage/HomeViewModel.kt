package com.example.spacedim.screens.Homepage

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.spacedim.modeles.User
import com.example.spacedim.network.UserApi
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

class HomeViewModel : ViewModel() {
    private val _user = MutableLiveData<User>()
    private val _connectionState = MutableLiveData<ConnectionState>()
    private val _registerState = MutableLiveData<RegisterState>()
    private val jsonObject = JSONObject()

    val user: LiveData<User>
        get() = _user

    val connectionState: LiveData<ConnectionState>
        get() = _connectionState

    val registerState: LiveData<RegisterState>
        get() = _registerState


    init {
        Log.i("HomeViewModel", "HomeViewModel created!")
    }

    fun getUserByName(username: String?){
        _connectionState.value = ConnectionState.TRY
        viewModelScope.launch {
            try {
                _user.value = UserApi.retrofitService.getUser(username)
                _connectionState.value = ConnectionState.SUCCESS
            } catch (e: Exception) {
                _connectionState.value = ConnectionState.FAIL
            }
        }
    }

    fun registerUserByUsername(username: String) {
        viewModelScope.launch {
            try {
                _registerState.value = RegisterState.TRY
                jsonObject.put("name", "$username")
                val jsonObjectString = jsonObject.toString()
                val requestBody = jsonObjectString.toRequestBody("application/json".toMediaTypeOrNull())
                val response = UserApi.retrofitService.registerUser(requestBody)
                when (response.code()) {
                    200 -> {
                        _registerState.value = RegisterState.SUCCESS
                        getUserByName(username)
                    }
                    401 -> {    _registerState.value = RegisterState.DOUBLE_USER    }
                    else -> {   _registerState.value = RegisterState.FAIL   }
                }
            } catch (e: Exception) {
                _registerState.value = RegisterState.FAIL
            }
        }

    }
}

enum class ConnectionState(val value: Int) {
    FAIL(0), TRY(1), SUCCESS(2)
}

enum class RegisterState(val value: Int) {
    FAIL(0), TRY(1), DOUBLE_USER(2),  SUCCESS(3)
}