package com.example.spacedim.screens.waitingroom

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.activity.addCallback
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import com.example.spacedim.GameViewModel
import com.example.spacedim.GameViewModelFactory
import com.example.spacedim.R
import com.example.spacedim.databinding.WaitingRoomBinding
import com.example.spacedim.modeles.*
import com.example.spacedim.screens.dashboard.DashboardFragmentDirections
import com.example.spacedim.screens.endGame.EndGameViewModelFactory
import com.google.android.material.button.MaterialButton


class WaitingRoomFragment : Fragment() {

    private lateinit var binding: WaitingRoomBinding

    private lateinit var viewModel: GameViewModel

    private lateinit var viewModelFactory: GameViewModelFactory

    private var room: String = ""

    var waitState = waitingStatus.UNJOINED

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        Log.i("WaitingRoomFragment", "onCreateView Called")

        requireActivity().onBackPressedDispatcher.addCallback(this){
            return@addCallback
        }
        viewModelFactory = GameViewModelFactory(
            WaitingRoomFragmentArgs.fromBundle(requireArguments()).incomingCurrentUserName,
            WaitingRoomFragmentArgs.fromBundle(requireArguments()).incomingCurrentUserId)

        viewModel = ViewModelProvider(requireActivity(), viewModelFactory).get(GameViewModel::class.java)

        // Inflate view and obtain an instance of the binding class
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.waiting_room,
            container,
            false
        )

        binding.gameViewModel = viewModel

        binding.lifecycleOwner = viewLifecycleOwner

        initScreen()
        viewModel.listener.eventMessage.observe(viewLifecycleOwner, Observer { mess: Event ->
            println(mess.type)
            if(mess.type == EventType.RESTART) {
                waitState = waitingStatus.UNJOINED
            } else {
                waitState = waitingStatus.JOINED

                updateScreen()

                    if (mess.type == EventType.WAITING_FOR_PLAYER) {
                        println("Event waiting")
                        val waiting = mess as Event.WaitingForPlayer
                        var switchToDashboard = true
                        binding.playersLayout.removeAllViews()

                        val userList: List<User> = waiting.userList
                        userList.forEach { user ->
                            if (user.state === State.WAITING)
                                switchToDashboard = false

                            binding.playersLayout.addView(createUserCard(user))
                        }

                        if (switchToDashboard && userList.size >= 2)
                            navToDashboard()
                    }
            }
        })

        binding.waitingJoinBtn.setOnClickListener {
            if (waitState === waitingStatus.UNJOINED) {
                val builder = AlertDialog.Builder(activity)
                val dialogInflater = layoutInflater
                builder.setTitle("Enter a room name")
                val dialogLayout = dialogInflater.inflate(R.layout.join_room_dialog, null)
                val dialogRoom = dialogLayout.findViewById<EditText>(R.id.roomText)
                builder.setView(dialogLayout)
                builder.setPositiveButton("OK") { _, _ ->
                    room = dialogRoom.text.toString()
                    viewModel.setRoom(room)
                    viewModel.joinRoom(room)
                }
                builder.show()
            } else if (waitState === waitingStatus.JOINED) {
                viewModel.setPlayerReady()
                waitState = waitingStatus.WAITING
            }
        }

        return binding.root
    }


    private fun navToDashboard() {
        waitState = waitingStatus.UNJOINED
        val action = WaitingRoomFragmentDirections.actionWaitingRoomFragmentToDashboardFragment()
        NavHostFragment.findNavController(this).navigate(action)
    }

    private fun createUserCard(user: User): LinearLayout {
        val cardInflater = LayoutInflater.from(this.context)
        val card = cardInflater.inflate(R.layout.user_card, null) as LinearLayout

        val username = card.findViewById<TextView>(R.id.player_name)
        val userStatus = card.findViewById<TextView>(R.id.player_connected)

        if (user.id === viewModel.getUserId())
            username.text = user.name + " (Me)"
        else
            username.text = user.name

        userStatus.text = user.state.name

        return card
    }

    @SuppressLint("ResourceAsColor")
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun updateScreen() {
        binding.waitingMainText.text = getString(R.string.waiting_connected_main_text)
        binding.waitingJoinBtn.text = getString(R.string.waiting_connected_btn)
        binding.roomName.text = "Room joined : $room"
        binding.socketStatusText.text = getString(R.string.waiting_connected_socket_on)
        context?.let { ContextCompat.getColor(it, R.color.socket_on) }?.let {
            binding.socketStatusBall.background.setTint(
                it
            )
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun initScreen() {
        binding.waitingMainText.text = getString(R.string.waiting_empty_main_text)
        binding.waitingJoinBtn.text = getString(R.string.waiting_empty_btn)
        binding.roomName.text = getString(R.string.waiting_empty_room)
        binding.socketStatusText.text = getString(R.string.waiting_empty_socket_off)
        context?.let { ContextCompat.getColor(it, R.color.socket_off) }?.let {
            binding.socketStatusBall.background.setTint(
                it
            )
        }
    }

}

enum class waitingStatus {
    WAITING, JOINED, UNJOINED
}
