package com.example.spacedim.screens.endGame

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import com.example.spacedim.R
import com.example.spacedim.EndGameViewModel
import com.example.spacedim.databinding.EndFragmentBinding
import com.example.spacedim.databinding.WaitingRoomBinding
import com.example.spacedim.modeles.*
import com.example.spacedim.screens.dashboard.DashboardFragmentDirections
import com.example.spacedim.screens.waitingroom.waitingStatus

class EndGameFragment : Fragment() {

    private lateinit var viewModel: EndGameViewModel
    private lateinit var viewModelFactory: EndGameViewModelFactory
    private lateinit var binding: EndFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        Log.i("EndGameFragment", "onCreateView Called")

        requireActivity().onBackPressedDispatcher.addCallback(this){
            return@addCallback
        }
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.end_fragment,
            container,
            false
        )

        viewModelFactory = EndGameViewModelFactory(EndGameFragmentArgs.fromBundle(requireArguments()).score,
                            EndGameFragmentArgs.fromBundle(requireArguments()).win,
                            EndGameFragmentArgs.fromBundle(requireArguments()).level
            )

        viewModel = ViewModelProvider(this, viewModelFactory)
            .get(EndGameViewModel::class.java)

        binding.endSecondText.text = "Tu as fini au level " + viewModel.level + " avec le score:"
        binding.score.text = viewModel.fScore.toString()

        if(viewModel.winStatus) {
            binding.endStatus.text = getString(R.string.winner)
            binding.endMainText.text = getString(R.string.you_win)
            binding.endImage.setImageResource(R.drawable.win)
            binding.retryBtn.text = getString(R.string.continue_winner)
        }

        binding.scoreBtn.setOnClickListener {
            val action = EndGameFragmentDirections.actionEndGameFragmentToScoresFragment()
            NavHostFragment.findNavController(this).navigate(action)
        }

        binding.retryBtn.setOnClickListener {
            val action = EndGameFragmentDirections.actionEndGameFragmentToHomeFragment()
            NavHostFragment.findNavController(this).navigate(action)
        }

        return binding.root


    }}