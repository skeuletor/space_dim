package com.example.spacedim.screens.endGame

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.spacedim.EndGameViewModel

class EndGameViewModelFactory(private val finalScore: Int, private val win: Boolean, private val level: Int) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(EndGameViewModel::class.java)) {
            return EndGameViewModel(finalScore, win, level) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}