package com.example.spacedim.screens.scores

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.spacedim.modeles.User
import com.example.spacedim.network.UserApi
import kotlinx.coroutines.launch

class ScoresViewModel: ViewModel() {

    private val _userList = MutableLiveData<List<User>>()
    val userList: LiveData<List<User>>
        get() = _userList
    init {
        getUsersScores()
    }

    fun getScores(): List<User>? {
        return userList.value
    }

    private fun getUsersScores () {
        viewModelScope.launch {
            try {
                _userList.value = UserApi.retrofitService.getBestScores()
                println("try ok ")
            } catch (e: Exception) {
                _userList.value = ArrayList()
                println("error ===> $e")
            }
        }
    }

}