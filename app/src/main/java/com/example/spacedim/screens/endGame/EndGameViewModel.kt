package com.example.spacedim

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class EndGameViewModel(finalScore: Int, win: Boolean, level: Int): ViewModel() {

    val fScore = finalScore
    val winStatus = win
    val level = level
    // The current endGame Word
    private val _endGameStatusWord = MutableLiveData<String>()
    val endGameStatusWord: LiveData<String>
        get() = _endGameStatusWord

    // The current Image
    private val _endGameStatusImage = MutableLiveData<String>()
    val endGameStatusImage: LiveData<String>
        get() = _endGameStatusImage

    private val _score = MutableLiveData<Int>()
    val score: LiveData<Int>
        get() = _score

    // Your team Text
    private val _teamStatusWord = MutableLiveData<String>()
    val teamStatusWord: LiveData<String>
        get() = _teamStatusWord

    init {
        Log.i("ScoreViewModel", "Your score is $finalScore, You win => $win, at level => $level")
        _score.value= finalScore
        println(score.value)
    }
}
