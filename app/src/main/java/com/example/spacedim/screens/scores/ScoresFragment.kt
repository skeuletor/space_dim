package com.example.spacedim.screens.scores

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ScrollView
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.spacedim.databinding.ScoresFragmentBinding
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.spacedim.R
import com.example.spacedim.modeles.State
import com.example.spacedim.modeles.User

class ScoresFragment : Fragment(){

    private val viewModel: ScoresViewModel by lazy {
        ViewModelProvider(this).get(ScoresViewModel::class.java)
    }

    private lateinit var binding: ScoresFragmentBinding

     override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
         binding = DataBindingUtil.inflate(
             inflater,
             R.layout.scores_fragment,
             container,
             false
         )

        binding.lifecycleOwner = this

         viewModel.userList.observe(viewLifecycleOwner, { userList ->

             println(userList)
             userList.forEach { user: User ->
                 binding.scoreLayout.addView(createUserCard(user))
             }

         })

        return binding.root
    }

    private fun createUserCard(user: User): LinearLayout {
        val inflater = LayoutInflater.from(this.context)
        val card = inflater.inflate(R.layout.user_card_score, null) as LinearLayout

        val username = card.findViewById<TextView>(R.id.player_name)
        val userStatus = card.findViewById<TextView>(R.id.player_score)

        username.text = user.name

        userStatus.text = user.score.toString() + " pts"

        return card
    }
}

