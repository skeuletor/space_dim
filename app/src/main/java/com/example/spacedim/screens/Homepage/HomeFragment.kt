package com.example.spacedim.screens.Homepage

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import com.example.spacedim.R
import com.example.spacedim.databinding.FragmentHomeBinding
import com.example.spacedim.modeles.User

class HomeFragment : Fragment() {
    private lateinit var viewModel: HomeViewModel

    private lateinit var currentUser: User

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        requireActivity().onBackPressedDispatcher.addCallback(this){
            return@addCallback
        }
        Log.i("HomeFragment", "onCreateView Called")
        viewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        val binding = DataBindingUtil.inflate<FragmentHomeBinding>(inflater,
            R.layout.fragment_home, container, false)

        viewModel.connectionState.observe(viewLifecycleOwner, {state ->
            when (state) {
                ConnectionState.FAIL -> {
                    binding.loginTxtConnexion.text = getString(R.string.login_msg_fail)
                    binding.loginTxtConnexion.setTextColor(Color.parseColor("#ff0000"))
                }
                ConnectionState.TRY -> {
                    binding.loginTxtConnexion.text = getString(R.string.login_msg_try)
                    binding.loginTxtConnexion.setTextColor(Color.parseColor("#ffffff"))
                }
                ConnectionState.SUCCESS -> {
                    binding.loginTxtConnexion.text = getString(R.string.login_msg_success)
                    binding.loginTxtConnexion.setTextColor(Color.parseColor("#00ff00"))
                    //view?.findNavController()?.navigate(R.id.action_homeFragment_to_waitingRoomFragment)
                    navToWaitingRoom()
                }
                else -> {
                    binding.loginTxtConnexion.text = ""
                }
            }
        })

        viewModel.registerState.observe(viewLifecycleOwner, { state ->
            when (state) {
                RegisterState.FAIL -> {
                    binding.loginTxtConnexion.text = getString(R.string.login_register_msg_fail)
                    binding.loginTxtConnexion.setTextColor(Color.parseColor("#ff0000"))
                }
                RegisterState.DOUBLE_USER -> {
                    binding.loginTxtConnexion.text = "${binding.loginRegisterName.text.toString()} " + getString(R.string.login_register_msg_double_user)
                    binding.loginTxtConnexion.setTextColor(Color.parseColor("#ff0000"))
                }
                RegisterState.TRY -> {
                    binding.loginTxtConnexion.text = getString(R.string.login_register_msg_try)
                    binding.loginTxtConnexion.setTextColor(Color.parseColor("#ffffff"))
                }
                RegisterState.SUCCESS -> {
                    binding.loginTxtConnexion.text = getString(R.string.login_register_msg_sucess)
                    binding.loginTxtConnexion.setTextColor(Color.parseColor("#00ff00"))
                    //view?.findNavController()?.navigate(R.id.action_homeFragment_to_waitingRoomFragment)
                    println("register success")
                }
                else -> {
                    binding.loginTxtConnexion.text = ""
                }
            }
        })

        viewModel.user.observe(viewLifecycleOwner, { user ->
                this.currentUser = user
        })

        binding.launchBtn.setOnClickListener{
            viewModel.getUserByName(binding.loginUsernameInput.text.toString())
            setUserName(binding.loginUsernameInput.text.toString())
        }
        binding.loginRegisterBtn.setOnClickListener {
            setUserName(binding.loginRegisterName.text.toString())
            viewModel.registerUserByUsername(binding.loginRegisterName.text.toString())
        }
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        binding.loginUsernameInput.setText(sharedPref?.getString("username", ""))

        return binding.root
    }

    fun setUserName(name: String){
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        with(sharedPref.edit()) {
            putString("username", name)
            apply()
        }
    }

    fun navToWaitingRoom() {
        val action = HomeFragmentDirections.actionHomeFragmentToWaitingRoomFragment(this.currentUser.name, this.currentUser.id)
//        action.incomingCurrentUserName = this.currentUser.name
        NavHostFragment.findNavController(this).navigate(action)
    }



}
